import React from "react";
import { Login } from "./pages/Login/login.jsx";
import { ShShPage } from "./pages/Shsh";
import "./index.less";
import { BrowserRouter as Router, Route } from "react-router-dom";
import localStorageItemKey from "./core/constants/localStorageItemKey.js";
import { Switch, Redirect } from "react-router";
import { Redirector } from "./shared/Redirector.jsx";
import { MainInstrumentalOperatorPage } from "./pages/MainInstrumenalOperator";
import { routes } from "./shared/routes";

const PrivateRoute = ({ children, ...rest }) => {
  const hasToken = Boolean(
    localStorage.getItem(localStorageItemKey.accessToken)
  );
  return (
    <Route
      {...rest}
      render={({ location }) =>
        hasToken ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: routes.login,
              state: {
                from: location,
              },
            }}
          />
        )
      }
    />
  );
};

const App = () => {
  return (
    <div style={{ height: "100vh", width: "100vw" }}>
      <Router>
        <Switch>
          <PrivateRoute path={routes.shsh}>
            <ShShPage />
          </PrivateRoute>
          <PrivateRoute path={routes.mainInstrumentOperator}>
            <MainInstrumentalOperatorPage />
          </PrivateRoute>
          <Route path={routes.login}>
            <Login />
          </Route>
          <Route path="*">
            <Redirector />
          </Route>
        </Switch>
      </Router>
    </div>
  );
};

export default App;
