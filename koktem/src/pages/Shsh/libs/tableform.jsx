import * as React from "react";
import { Form, Input, Table } from "antd";

const { Column } = Table;

export const EditableTable = (props) => {
  const { values, loading, readOnly } = props;

  return (
    <Table dataSource={values} pagination={false} loading={loading}>
      <Column dataIndex={"name"} title={"Название"} key={"name"} />
      <Column
        dataIndex={"value"}
        title={"Значение"}
        key={"value"}
        render={(value, row) => {
          return (
            <Form.Item
              name={row.key}
              rules={[
                {
                  required: true,
                  message: "A value must be entered",
                  pattern: new RegExp(/^[+-]?\d+(\.\d+)?$/),
                },
              ]}
            >
              <Input
                readOnly={readOnly}
                placeholder="Значение"
                style={{ width: "50%", marginRight: 8 }}
              />
            </Form.Item>
          );
        }}
      />
    </Table>
  );
};
