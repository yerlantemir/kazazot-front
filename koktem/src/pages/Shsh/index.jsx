import React, { useEffect, useState } from "react";
import axios from "../../core/networking/koktem-axios.js";
import { EditableTable } from "./libs/tableform";
import { Form, Button, Select, notification } from "antd";
import { shshDict as dictionary } from "../../core/constants/form-keys/shshFormKeys";
import localStorageItemKey from "../../core/constants/localStorageItemKey.js";
import role from "../../core/constants/roles.js";
import Text from "antd/es/typography/Text";
import { Layout } from "../../shared/ui/Layout";

export const ShShPage = () => {
  const { Option } = Select;
  const [isLoading, setLoading] = useState(false);
  const [isSubmitButtonHidden, setSubmitButtonHidden] = useState(false);

  const currentYear = new Date().getFullYear();
  const currentQuarter = Math.floor((new Date().getMonth() + 3) / 3);
  const userRole = localStorage.getItem(localStorageItemKey.role);
  const [tableId, setTableId] = useState();
  const canSubmit = userRole === role.shsh || userRole === role.ecolog;
  const [isReadOnly, setReadOnly] = useState(!canSubmit);

  let selectedYear = currentYear;
  let selectedQuarter = currentQuarter;

  const data = Object.keys(dictionary).map((key) => {
    return {
      name: dictionary[key],
      key,
      value: null,
    };
  });

  const [tableData, setTableData] = useState(data);
  const [form] = Form.useForm();
  const onFinish = (values) => {
    setLoading(true);
    if (!tableId) {
      axios({
        url: "/shsh",
        method: "post",
        data: values,
      })
        .then(() => {
          notification.success({
            message: "Данные сохранены",
          });
        })
        .catch(() => {
          notification.error({
            message: "Произошла ошибка на серввер",
          });
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      axios({
        url: "/shsh",
        method: "patch",
        data: values,
        params: { id: "1" },
      })
        .then(() => {
          notification.success({
            message: "Даннные изменены",
          });
        })
        .catch(() => {
          notification.error({
            message: "Произошла ошибка на сервере",
          });
        })
        .finally(() => {
          setLoading(false);
        });
    }
  };

  const yearOptionComponents = () => {
    const min = currentYear - 2;
    let years = [];
    for (let i = currentYear; i >= min; i--) {
      years.push(i);
    }
    return years.map((year) => {
      return optionComponent(year);
    });
  };

  const quarterOptionComponents = () => {
    const max = 4;
    const min = 1;
    let quarters = [];
    for (let i = max; i >= min; i--) {
      quarters.push(i);
    }
    return quarters.map((quarter) => {
      return optionComponent(quarter);
    });
  };

  const optionComponent = (option) => {
    return <Option value={option}>{option}</Option>;
  };

  const handleYearChange = (value) => {
    selectedYear = value;
    setSubmitButtonHidden(
      canSubmit &&
        (selectedYear !== currentYear || selectedQuarter !== currentQuarter)
    );
    setReadOnly(
      canSubmit &&
        (selectedYear !== currentYear || selectedQuarter !== currentQuarter)
    );
    loadData();
  };

  const handleMonthChange = (value) => {
    selectedQuarter = value;
    setSubmitButtonHidden(
      canSubmit &&
        (selectedYear !== currentYear || selectedQuarter !== currentQuarter)
    );
    setReadOnly(
      canSubmit &&
        (selectedYear !== currentYear || selectedQuarter !== currentQuarter)
    );
    loadData();
  };

  const loadData = () => {
    setLoading(true);
    axios({
      url: "/shsh",
      method: "get",
      params: {
        year: selectedYear,
        quarter: selectedQuarter,
      },
    })
      .then((response) => {
        setLoading(false);
        const data = Object.keys(dictionary).map((key) => {
          return {
            name: dictionary[key],
            key,
            value: response.data[key],
          };
        });
        setTableId(response.data.id);
        setTableData(data);
        form.setFieldsValue(response.data);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    loadData();
  }, []);
  return (
    <Layout>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <div style={{ display: "flex", flexDirection: "column" }}>
          <Text level={5}>Год</Text>
          <Select defaultValue={currentYear} onChange={handleYearChange}>
            {yearOptionComponents()}
          </Select>
        </div>
        <div
          style={{ display: "flex", flexDirection: "column", marginLeft: 20 }}
        >
          <Text level={5}>Квартал</Text>
          <Select defaultValue={currentQuarter} onChange={handleMonthChange}>
            {quarterOptionComponents()}
          </Select>
        </div>
      </div>
      <Form form={form} onFinish={onFinish} style={{ marginTop: 10 }}>
        <EditableTable
          values={tableData}
          loading={isLoading}
          readOnly={isReadOnly}
        />
        <Form.Item>
          <Button
            loading={isLoading}
            style={{ margin: "20px 0px 0px 0px", height: "60px" }}
            size="large"
            type="primary"
            htmlType="submit"
            hidden={!canSubmit || isSubmitButtonHidden}
            block
          >
            Отправить
          </Button>
        </Form.Item>
      </Form>
    </Layout>
  );
};
