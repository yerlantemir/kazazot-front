import { Layout } from "../../shared/ui/Layout";
import { Tabs } from "antd";
import { Metering } from "./libs/Metering/Metering";
import { Discharge } from "./libs/Discharge/Discharge";
import { Consumption } from "./libs/WaterConsumption/WaterConsumption";

const { TabPane } = Tabs;

export const MainInstrumentalOperatorPage = () => {
  function callback(key) {
    console.log(key);
  }
  return (
    <Layout>
      <Tabs defaultActiveKey="1" onChange={callback}>
        <TabPane tab="Проверка приборов учета" key="1">
          <Metering />
        </TabPane>
        <TabPane
          tab="Фактический сброс нормативно-чистых вод в сбросной"
          key="2"
        >
          <Discharge />
        </TabPane>
        <TabPane tab="Фактическое потребление технической воды" key="3">
          <Consumption />
        </TabPane>
      </Tabs>
    </Layout>
  );
};
