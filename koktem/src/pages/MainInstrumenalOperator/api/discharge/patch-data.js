import configuredAxios from "../../../../core/networking/koktem-axios";

export const patchDischarge = async ({ id, date, count }) => {
  const resp = await configuredAxios.post(
    "/ins-operator/discharge",
    {
      date,
      count,
    },
    {
      params: {
        id,
      },
    }
  );

  return resp.data;
};
