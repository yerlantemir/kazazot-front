import configuredAxios from "../../../../core/networking/koktem-axios";

export const postDischarge = async ({ date, count }) => {
  const resp = await configuredAxios.post("/ins-operator/discharge", {
    date,
    count,
  });

  return resp.data;
};
