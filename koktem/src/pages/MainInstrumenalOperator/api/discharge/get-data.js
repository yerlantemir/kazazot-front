import configuredAxios from "../../../../core/networking/koktem-axios";

export const getDischargeData = async ({ year, quarter }) => {
  const resp = await configuredAxios.get("/ins-operator/discharge", {
    params: {
      year,
      quarter,
    },
  });
  return resp.data;
};
