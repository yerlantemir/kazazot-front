import configuredAxios from "../../../../core/networking/koktem-axios";

export const postConsumption = async ({ waterTypeId, count }) => {
  const resp = await configuredAxios.post("/ins-operator/water-consumption", {
    waterTypeId,
    count,
  });

  return resp.data;
};
