import configuredAxios from "../../../../core/networking/koktem-axios";

export const getConsumptionData = async ({ year, quarter }) => {
  const resp = await configuredAxios.get("/ins-operator/water-consumption", {
    params: {
      year,
      quarter,
    },
  });
  return resp.data;
};
