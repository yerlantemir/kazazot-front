import configuredAxios from "../../../../core/networking/koktem-axios";

export const patchConsumption = async ({ id, waterTypeId, count }) => {
  const resp = await configuredAxios.patch(
    "/ins-operator/water-consumption",
    {
      waterTypeId,
      count,
    },
    {
      params: {
        id,
      },
    }
  );

  return resp.data;
};
