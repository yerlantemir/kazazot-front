import configuredAxios from "../../../../core/networking/koktem-axios";

export const getWaterTypes = async () => {
  const resp = await configuredAxios.get(
    "/ins-operator/water-consumption-type"
  );
  return resp.data;
};
