import configuredAxios from "../../../../core/networking/koktem-axios";

export const getMeteringDevicesType = async () => {
  const resp = await configuredAxios.get("/ins-operator/metering-devices-type");
  return resp.data;
};
