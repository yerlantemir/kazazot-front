import configuredAxios from "../../../../core/networking/koktem-axios";

export const getMeteringData = async ({ year, quarter }) => {
  const resp = await configuredAxios.get("/ins-operator/metering", {
    params: {
      year,
      quarter,
    },
  });
  return resp.data;
};
