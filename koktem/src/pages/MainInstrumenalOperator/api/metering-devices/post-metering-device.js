import configuredAxios from "../../../../core/networking/koktem-axios";

export const postMeteringDevice = async ({ applianceId, date, number }) => {
  const resp = await configuredAxios.post("/ins-operator/metering", {
    applianceId,
    examinationDate: date,
    documentNumber: number,
  });

  return resp.data;
};
