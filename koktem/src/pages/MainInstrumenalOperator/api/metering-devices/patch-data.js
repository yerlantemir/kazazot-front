import configuredAxios from "../../../../core/networking/koktem-axios";

export const patchMetering = async ({ id, applianceId, date, number }) => {
  const resp = await configuredAxios.patch(
    "/ins-operator/metering",
    {
      applianceId,
      examinationDate: date,
      documentNumber: number,
    },
    {
      params: {
        id,
      },
    }
  );

  return resp.data;
};
