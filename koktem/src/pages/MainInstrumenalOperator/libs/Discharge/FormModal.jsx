import {
  Button,
  DatePicker,
  Form,
  InputNumber,
  Modal,
  notification,
} from "antd";
import { useState } from "react";
import { postDischarge } from "../../api/discharge/post-discharge";

const requiredRule = {
  required: true,
  message: "Обятазельное поле",
};
export const FormModal = ({ modalActive, closeModal, refetch }) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const onAddSubmit = (values) => {
    setLoading(true);
    postDischarge({
      date: values.date,
      count: values.count,
    })
      .then(() => {
        notification.success({
          message: "Отчет успешно создан",
        });
      })
      .catch(() => {
        notification.error({
          message: "Произошла ошибка",
        });
      })
      .finally(() => {
        closeModal(false);
        setLoading(false);
        refetch();
      });
  };
  return (
    <Modal
      visible={modalActive}
      title={"Добавить отчет"}
      style={{ minWidth: "800px" }}
      footer={null}
      onCancel={() => {
        closeModal(false);
      }}
    >
      <Form onFinish={onAddSubmit} form={form} layout={"vertical"}>
        <div style={{ display: "flex" }}>
          <Form.Item
            label={"Дата"}
            name={"date"}
            style={{ flexGrow: 1, marginLeft: 10 }}
            rules={[requiredRule]}
          >
            <DatePicker
              style={{ width: "100%" }}
              placeholder={"выберите дату"}
            />
          </Form.Item>
          <Form.Item
            label={"Кол-во сброса"}
            name={"count"}
            style={{ flexGrow: 1, marginLeft: "10px" }}
            rules={[requiredRule]}
          >
            <InputNumber min={0} placeholder={"напишите"} />
          </Form.Item>
        </div>
        <div style={{ display: "flex", justifyContent: "flex-end" }}>
          <Button loading={loading} type={"primary"} htmlType={"submit"}>
            Добавить
          </Button>
        </div>
      </Form>
    </Modal>
  );
};
