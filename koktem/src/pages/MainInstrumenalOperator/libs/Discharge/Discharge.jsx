import { useEffect, useState } from "react";
import {
  Button,
  DatePicker,
  Form,
  InputNumber,
  Popconfirm,
  Spin,
  Table,
  Typography,
} from "antd";
import { FormModal } from "./FormModal";
import moment from "moment";
import { dateFormat } from "../../../../core/constants/constants";
import { getDischargeData } from "../../api/discharge/get-data";
import { patchDischarge } from "../../api/discharge/patch-data";

const EditableCell = ({
  editing,
  dataIndex,
  title,
  inputType,
  children,
  ...restProps
}) => {
  let inputNode;
  if (inputType === "count") {
    inputNode = <InputNumber />;
  } else if (inputType === "date") {
    inputNode = <DatePicker format={dateFormat} />;
  }

  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{ margin: 0 }}
          rules={[
            {
              required: true,
              message: `Пожалуйста, введите ${title}!`,
            },
          ]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

export const Discharge = () => {
  const [modalActive, setModalActive] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [loading, setLoading] = useState(false);

  const [form] = Form.useForm();

  const [editingKey, setEditingKey] = useState("");
  const isEditing = (record) => record.key === editingKey;

  const edit = (record) => {
    form.setFieldsValue({ name: "", age: "", address: "", ...record });
    setEditingKey(record.key);
  };
  const cancel = () => {
    setEditingKey("");
  };
  const save = async (key) => {
    setLoading(true);
    try {
      const row = await form.validateFields();

      const newData = [...tableData];
      const index = newData.findIndex((item) => key === item.key);
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        setTableData(newData);
        setEditingKey("");
        await patchDischarge({
          id: item.id,
          date: row.date.toString(),
          count: row.count,
        });
      }
    } catch (errInfo) {
      console.log("Validate Failed:", errInfo);
    }
    setLoading(false);
  };

  const tableColumns = [
    {
      title: "Дата проверки",
      dataIndex: "date",
      key: "date",
      editable: true,
      render: (_, record) => {
        return <span>{moment(record.date).format(dateFormat)}</span>;
      },
    },
    {
      title: "Количество",
      dataIndex: "count",
      key: "count",
      editable: true,
    },
    {
      title: "operation",
      dataIndex: "operation",
      render: (_, record) => {
        const editable = isEditing(record);
        if (loading) {
          return <Spin />;
        }

        return editable ? (
          <span>
            <Typography.Link
              onClick={() => save(record.key)}
              style={{ marginRight: 8 }}
            >
              Сохранить
            </Typography.Link>
            <Popconfirm
              title="Вы уверены что хотите отменить ?"
              onConfirm={cancel}
            >
              <a>Отменить</a>
            </Popconfirm>
          </span>
        ) : (
          <Typography.Link
            disabled={editingKey !== ""}
            onClick={() => edit(record)}
          >
            Редактировать
          </Typography.Link>
        );
      },
    },
  ];

  const mergedColumns = tableColumns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record) => ({
        record,
        inputType: col.dataIndex,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });
  const updateDischargeData = () => {
    setLoading(true);
    getDischargeData({ year: 2021, quarter: 4 })
      .then((data) => {
        setTableData(
          data.map((d) => {
            return {
              date: moment(d.date),
              count: d.count,
            };
          })
        );
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    updateDischargeData();
  }, []);
  return (
    <div style={{ padding: "10px 100px" }}>
      <FormModal
        modalActive={modalActive}
        closeModal={() => setModalActive(false)}
        refetch={updateDischargeData}
      />
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button
          onClick={() => {
            setModalActive(true);
          }}
          loading={loading}
        >
          + Добавить
        </Button>
      </div>
      <Form form={form} component={false} style={{ paddingTop: 50 }}>
        <Table
          bordered
          components={{
            body: {
              cell: EditableCell,
            },
          }}
          columns={mergedColumns}
          pagination={{
            onChange: cancel,
          }}
          dataSource={tableData}
        />
      </Form>
    </div>
  );
};
