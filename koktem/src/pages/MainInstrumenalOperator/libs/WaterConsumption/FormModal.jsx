import { Button, Form, InputNumber, Modal, notification, Select } from "antd";
import { useState } from "react";
import { postConsumption } from "../../api/waterConsumption/post-consumption";

const requiredRule = {
  required: true,
  message: "Обятазельное поле",
};
export const FormModal = ({ modalActive, closeModal, refetch, waterTypes }) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const onAddSubmit = (values) => {
    setLoading(true);
    postConsumption({
      waterTypeId: values.waterTypeId,
      count: values.count,
    })
      .then(() => {
        notification.success({
          message: "Отчет успешно создан",
        });
      })
      .catch(() => {
        notification.error({
          message: "Произошла ошибка",
        });
      })
      .finally(() => {
        closeModal(false);
        setLoading(false);
        refetch();
      });
  };
  return (
    <Modal
      visible={modalActive}
      title={"Добавить отчет"}
      style={{ minWidth: "800px" }}
      footer={null}
      onCancel={() => {
        closeModal(false);
      }}
    >
      <Form onFinish={onAddSubmit} form={form} layout={"vertical"}>
        <div style={{ display: "flex" }}>
          <Form.Item
            name={"waterTypeId"}
            label={"Тип воды"}
            style={{ flexGrow: 1 }}
            rules={[requiredRule]}
          >
            <Select placeholder={"Выберите"}>
              {waterTypes?.map((device) => {
                return (
                  <Select.Option value={device.id} key={device.id}>
                    {device.name}{" "}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item
            label={"Количество"}
            name={"count"}
            style={{ flexGrow: 1, marginLeft: "10px" }}
            rules={[requiredRule]}
          >
            <InputNumber placeholder={"напишите"} />
          </Form.Item>
        </div>
        <div style={{ display: "flex", justifyContent: "flex-end" }}>
          <Button loading={loading} type={"primary"} htmlType={"submit"}>
            Добавить
          </Button>
        </div>
      </Form>
    </Modal>
  );
};
