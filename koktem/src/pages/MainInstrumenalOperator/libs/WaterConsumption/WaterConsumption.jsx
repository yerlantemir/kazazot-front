import { createContext, useContext, useEffect, useState } from "react";
import {
  Button,
  Form,
  InputNumber,
  Popconfirm,
  Select,
  Table,
  Typography,
} from "antd";
import { FormModal } from "./FormModal";
import { getWaterTypes } from "../../api/waterConsumption/get-water-types";
import { getConsumptionData } from "../../api/waterConsumption/get-data";

const WaterTypesContext = createContext({ waterTypes: [] });
const EditableCell = ({
  editing,
  dataIndex,
  title,
  inputType,
  children,
  ...restProps
}) => {
  let inputNode;
  const { waterTypes } = useContext(WaterTypesContext);
  if (inputType === "count") {
    inputNode = <InputNumber />;
  } else if (inputType === "type") {
    inputNode = (
      <Select>
        {waterTypes.map((option) => (
          <Select.Option value={option.id} key={option.id}>
            {option.name}
          </Select.Option>
        ))}
      </Select>
    );
  }

  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{ margin: 0 }}
          rules={[
            {
              required: true,
              message: `Пожалуйста, введите ${title}!`,
            },
          ]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

export const Consumption = () => {
  const [waterTypes, setWaterTypes] = useState([]);
  const [modalActive, setModalActive] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getWaterTypes().then((data) => {
      setWaterTypes(data);
    });
  }, []);

  const [form] = Form.useForm();

  const [editingKey, setEditingKey] = useState("");
  const isEditing = (record) => record.key === editingKey;

  const edit = (record) => {
    form.setFieldsValue({ name: "", age: "", address: "", ...record });
    setEditingKey(record.key);
  };
  const cancel = () => {
    setEditingKey("");
  };
  const save = async (key) => {
    try {
      const row = await form.validateFields();

      const newData = [...tableData];
      const index = newData.findIndex((item) => key === item.key);
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        setTableData(newData);
        setEditingKey("");
      } else {
        newData.push(row);
        setTableData(newData);
        setEditingKey("");
      }
    } catch (errInfo) {
      console.log("Validate Failed:", errInfo);
    }
  };

  const tableColumns = [
    {
      title: "Тип воды",
      dataIndex: "type",
      key: "type",
      editable: true,
    },
    {
      title: "Количество",
      dataIndex: "count",
      key: "count",
      editable: true,
    },
    {
      title: "operation",
      dataIndex: "operation",
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <Typography.Link
              onClick={() => save(record.key)}
              style={{ marginRight: 8 }}
            >
              Save
            </Typography.Link>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              <a>Cancel</a>
            </Popconfirm>
          </span>
        ) : (
          <Typography.Link
            disabled={editingKey !== ""}
            onClick={() => edit(record)}
          >
            Edit
          </Typography.Link>
        );
      },
    },
  ];

  const mergedColumns = tableColumns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record) => ({
        record,
        inputType: col.dataIndex,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });
  const updateConsumptionData = () => {
    setLoading(true);
    getConsumptionData({ year: 2021, quarter: 4 })
      .then((data) => {
        setTableData(
          data.map((d) => {
            return {
              type: waterTypes.find((dt) => dt.id === d.waterTypeId)?.name,
              count: d.count,
            };
          })
        );
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    updateConsumptionData();
  }, [waterTypes]);
  return (
    <div style={{ padding: "10px 100px" }}>
      <FormModal
        modalActive={modalActive}
        closeModal={() => setModalActive(false)}
        waterTypes={waterTypes}
        refetch={updateConsumptionData}
      />
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button
          onClick={() => {
            setModalActive(true);
          }}
          loading={loading}
        >
          + Добавить
        </Button>
      </div>
      <Form form={form} component={false} style={{ paddingTop: 50 }}>
        <WaterTypesContext.Provider value={{ waterTypes }}>
          <Table
            bordered
            components={{
              body: {
                cell: EditableCell,
              },
            }}
            columns={mergedColumns}
            pagination={{
              onChange: cancel,
            }}
            dataSource={tableData}
          />
        </WaterTypesContext.Provider>
      </Form>
    </div>
  );
};
