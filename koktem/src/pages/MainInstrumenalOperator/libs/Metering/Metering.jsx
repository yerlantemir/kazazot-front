import { getMeteringDevicesType } from "../../api/metering-devices/get-metering-devices-type";
import { createContext, useContext, useEffect, useState } from "react";
import {
  Button,
  DatePicker,
  Form,
  Input,
  Popconfirm,
  Select,
  Spin,
  Table,
  Typography,
} from "antd";
import { getMeteringData } from "../../api/metering-devices/get-data";
import { FormModal } from "./FormModal";
import moment from "moment";
import { dateFormat } from "../../../../core/constants/constants";
import { patchMetering } from "../../api/metering-devices/patch-data";

const DevicesContext = createContext({ devices: [] });
const EditableCell = ({
  editing,
  dataIndex,
  title,
  inputType,
  children,
  ...restProps
}) => {
  let inputNode;
  const { devices } = useContext(DevicesContext);
  if (inputType === "doc") {
    inputNode = <Input />;
  } else if (inputType === "date") {
    inputNode = <DatePicker format={dateFormat} />;
  } else if (inputType === "type") {
    inputNode = (
      <Select>
        {devices.map((option) => (
          <Select.Option value={option.id} key={option.id}>
            {option.name}
          </Select.Option>
        ))}
      </Select>
    );
  }

  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{ margin: 0 }}
          rules={[
            {
              required: true,
              message: `Пожалуйста, введите ${title}!`,
            },
          ]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

export const Metering = () => {
  const [devicesType, setDevicesType] = useState([]);
  const [modalActive, setModalActive] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getMeteringDevicesType().then((data) => {
      setDevicesType(data);
    });
  }, []);

  const [form] = Form.useForm();

  const [editingKey, setEditingKey] = useState("");
  const isEditing = (record) => record.key === editingKey;

  const edit = (record) => {
    form.setFieldsValue({ name: "", age: "", address: "", ...record });
    setEditingKey(record.key);
  };
  const cancel = () => {
    setEditingKey("");
  };
  const save = async (key) => {
    setLoading(true);
    try {
      const row = await form.validateFields();

      const newData = [...tableData];
      const index = newData.findIndex((item) => key === item.key);
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        setTableData(newData);
        setEditingKey("");
        await patchMetering({
          id: item.id,
          applianceId: devicesType.find((d) => d.name === row.type).id,
          date: row.date.toString(),
          number: row.doc,
        });
      }
    } catch (errInfo) {
      console.log("Validate Failed:", errInfo);
    }
    setLoading(false);
  };

  const tableColumns = [
    {
      title: "Тип прибора",
      dataIndex: "type",
      key: "type",
      editable: true,
    },
    {
      title: "Дата проверки",
      dataIndex: "date",
      key: "date",
      editable: true,
      render: (_, record) => {
        return <span>{moment(record.date).format(dateFormat)}</span>;
      },
    },
    {
      title: "Номер документа",
      dataIndex: "doc",
      key: "doc",
      editable: true,
    },
    {
      title: "operation",
      dataIndex: "operation",
      render: (_, record) => {
        const editable = isEditing(record);
        if (loading) {
          return <Spin />;
        }
        return editable ? (
          <span>
            <Typography.Link
              onClick={() => save(record.key)}
              style={{ marginRight: 8 }}
            >
              Save
            </Typography.Link>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              <a>Cancel</a>
            </Popconfirm>
          </span>
        ) : (
          <Typography.Link
            disabled={editingKey !== ""}
            onClick={() => edit(record)}
          >
            Edit
          </Typography.Link>
        );
      },
    },
  ];

  const mergedColumns = tableColumns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record) => ({
        record,
        inputType: col.dataIndex,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });
  const updateMeteringDevices = () => {
    setLoading(true);
    getMeteringData({ year: 2021, quarter: 4 })
      .then((data) => {
        setTableData(
          data.map((d) => {
            return {
              id: d.id,
              type: devicesType.find((dt) => dt.id === d.applianceId)?.name,
              date: moment(d.examination_date),
              doc: d.document_number,
            };
          })
        );
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    updateMeteringDevices();
  }, [devicesType]);
  return (
    <div style={{ padding: "10px 100px" }}>
      <FormModal
        modalActive={modalActive}
        closeModal={() => setModalActive(false)}
        devices={devicesType}
        refetch={updateMeteringDevices}
      />
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button
          onClick={() => {
            setModalActive(true);
          }}
          loading={loading}
        >
          + Добавить
        </Button>
      </div>
      <Form form={form} component={false} style={{ paddingTop: 50 }}>
        <DevicesContext.Provider value={{ devices: devicesType }}>
          <Table
            bordered
            components={{
              body: {
                cell: EditableCell,
              },
            }}
            columns={mergedColumns}
            pagination={{
              onChange: cancel,
            }}
            dataSource={tableData}
          />
        </DevicesContext.Provider>
      </Form>
    </div>
  );
};
