import {
  Button,
  DatePicker,
  Form,
  Input,
  Modal,
  notification,
  Select,
} from "antd";
import { useState } from "react";
import { postMeteringDevice } from "../../api/metering-devices/post-metering-device";

const requiredRule = {
  required: true,
  message: "Обятазельное поле",
};
export const FormModal = ({ modalActive, closeModal, refetch, devices }) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const onAddSubmit = (values) => {
    setLoading(true);
    postMeteringDevice({
      applianceId: values.applianceId,
      date: values.date,
      number: values.document,
    })
      .then(() => {
        notification.success({
          message: "Отчет успешно создан",
        });
      })
      .catch(() => {
        notification.error({
          message: "Произошла ошибка",
        });
      })
      .finally(() => {
        closeModal(false);
        setLoading(false);
        refetch();
      });
  };
  return (
    <Modal
      visible={modalActive}
      title={"Добавить отчет"}
      style={{ minWidth: "800px" }}
      footer={null}
      onCancel={() => {
        closeModal(false);
      }}
    >
      <Form onFinish={onAddSubmit} form={form} layout={"vertical"}>
        <div style={{ display: "flex" }}>
          <Form.Item
            name={"applianceId"}
            label={"Тип прибора"}
            style={{ flexGrow: 1 }}
            rules={[requiredRule]}
          >
            <Select placeholder={"Выберите"}>
              {devices?.map((device) => {
                return (
                  <Select.Option value={device.id} key={device.id}>
                    {device.name}{" "}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item
            label={"Дата проверки"}
            name={"date"}
            style={{ flexGrow: 1, marginLeft: 10 }}
            rules={[requiredRule]}
          >
            <DatePicker
              style={{ width: "100%" }}
              placeholder={"выберите дату"}
            />
          </Form.Item>
          <Form.Item
            label={"Номер документа"}
            name={"document"}
            style={{ flexGrow: 1, marginLeft: "10px" }}
            rules={[requiredRule]}
          >
            <Input placeholder={"напишите"} />
          </Form.Item>
        </div>
        <div style={{ display: "flex", justifyContent: "flex-end" }}>
          <Button loading={loading} type={"primary"} htmlType={"submit"}>
            Добавить
          </Button>
        </div>
      </Form>
    </Modal>
  );
};
