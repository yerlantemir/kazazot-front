import {
  Col,
  Input,
  Row,
  Form,
  Button,
  Checkbox,
  Image,
  Divider,
  Tabs,
} from "antd";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import { UserOutlined } from "@ant-design/icons";
import React, { useState } from "react";
import "./login.css";
import logo from "./logo.png";
import axios from "axios";
import { useHistory } from "react-router";
import localStorageItemKey from "../../core/constants/localStorageItemKey";

export const Login = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);

  const { TabPane } = Tabs;

  const [form] = Form.useForm();
  const onFinish = async (values) => {
    const username = values["username"];
    const password = values["password"];
    // eslint-disable-next-line no-undef
    const loginEndpoint = `${process.env.REACT_APP_ENDPOINT_URL}/auth/login`;
    setLoading(true);
    axios({
      method: "post",
      url: loginEndpoint,
      data: { username, password },
    })
      .then((response) => {
        const token = response.data["data"]["token"];
        const role = response.data["data"]["role"];
        localStorage.setItem(localStorageItemKey.id, username);
        if (token) {
          localStorage.setItem(localStorageItemKey.accessToken, token);
        }
        if (role) {
          localStorage.setItem(localStorageItemKey.role, role);
          history.push(`/${role}`);
        }
        history.push("/");
      })
      .catch((error) => {
        console.log(`auth/login error: ${error}`);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <Row
      align="middle"
      justify="space-around"
      style={{ height: "100vh", backgroundColor: "#F0F2F5" }}
    >
      <Col span={8}>
        <Form
          className="login-form"
          initialValues={{ remember: false }}
          onFinish={onFinish}
          size="large"
          form={form}
        >
          <Form.Item>
            <Image preview={false} src={logo} />
          </Form.Item>
          <Form.Item>
            <Tabs defaultActiveKey="1" size="small">
              <TabPane tab="Вход в систему" key="1" />
            </Tabs>
            <Divider className="login-form-divider" style={{ margin: "0px" }} />
          </Form.Item>
          <Form.Item name="username">
            <Input
              span={24}
              placeholder="Введите имя пользователя"
              prefix={<UserOutlined className="site-form-item-icon" />}
            />
          </Form.Item>
          <Form.Item name="password">
            <Input.Password
              span={24}
              placeholder="Введите пароль"
              iconRender={(visible) =>
                visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
              }
            />
          </Form.Item>
          <Form.Item>
            <Form.Item name="remember" valuePropName="checked" noStyle>
              <Checkbox>Запомнить меня</Checkbox>
            </Form.Item>
            <a className="login-form-forgot" href="">
              Забыли пароль?
            </a>
          </Form.Item>
          <Form.Item>
            <Button
              className="login-submit login-form-button"
              type="primary"
              htmlType="submit"
              block
              loading={loading}
            >
              Войти
            </Button>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};
