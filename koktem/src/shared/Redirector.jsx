import localStorageItemKey from "../core/constants/localStorageItemKey.js";
import { Redirect } from "react-router";
import roles from "../core/constants/roles";
import { routes } from "./routes";

export const Redirector = () => {
  const token = localStorage.getItem(localStorageItemKey.accessToken);
  const role = localStorage.getItem(localStorageItemKey.role);
  if (token) {
    switch (role) {
      case roles.shsh:
        return (
          <Redirect
            to={{
              pathname: routes.shsh,
            }}
          />
        );

      case roles.ecolog:
        return (
          <Redirect
            to={{
              pathname: routes.shsh,
            }}
          />
        );
      case roles.accountant:
        return (
          <Redirect
            to={{
              pathname: routes.shsh,
            }}
          />
        );
      case roles.mainInstrumentOperator:
        return (
          <Redirect
            to={{
              pathname: routes.mainInstrumentOperator,
            }}
          />
        );
      default:
        console.log("qwe?");
        return (
          <Redirect
            to={{
              pathname: "/login",
            }}
          />
        );
    }
  } else {
    return (
      <Redirect
        to={{
          pathname: "/login",
        }}
      />
    );
  }
};
