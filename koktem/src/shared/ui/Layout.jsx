import { Image, Layout as AntdLayout, Menu } from "antd";
import logo from "../../pages/Login/logo.png";
import React, { useMemo } from "react";
import Sider from "antd/es/layout/Sider";
import localStorageItemKey from "../../core/constants/localStorageItemKey";
import roles from "../../core/constants/roles";
import { Content } from "antd/es/layout/layout";
import { useHistory, useLocation } from "react-router-dom/cjs/react-router-dom";
import { routes } from "../routes";

export const Layout = ({ children, ...rest }) => {
  const location = useLocation();

  const userRole = localStorage.getItem(localStorageItemKey.role);
  const history = useHistory();
  const handleMenuItemKeySelection = (key) => {
    if (key === "exit") {
      return;
    }
    history.push(key);
  };

  const logout = () => {
    localStorage.clear();
    history.push("/");
  };

  const selectedKeys = useMemo(() => {
    return Object.values(routes).find((val) => location.pathname.includes(val));
  }, []);
  console.log(selectedKeys);
  return (
    <AntdLayout style={{ minHeight: "100vh", overflowY: "hidden" }} {...rest}>
      <Sider
        theme="light"
        breakpoint="lg"
        collapsedWidth="0"
        style={{
          maxHeight: "100vh",
          minWidth: 250,
        }}
      >
        <Image preview={false} src={logo} style={{ margin: "40px 0px" }} />
        <Menu
          onClick={(e) => {
            handleMenuItemKeySelection(e.key);
          }}
          style={{ maxHeight: "100%" }}
          selectedKeys={selectedKeys}
        >
          {[roles.ecolog, roles.shsh, roles.accountant].includes(userRole) && (
            <Menu.Item key={routes.shsh}>Шагырлы Шамышты</Menu.Item>
          )}
          {[
            roles.ecolog,
            roles.mainInstrumentOperator,
            roles.accountant,
          ].includes(userRole) && (
            <Menu.Item key={routes.mainInstrumentOperator}>
              Отдел главного прибориста
            </Menu.Item>
          )}
          <Menu.Item
            style={{
              position: "fixed",
              bottom: 0,
            }}
            key="exit"
            onClick={logout}
          >
            Выход
          </Menu.Item>
        </Menu>
      </Sider>

      <Content style={{ padding: 40, minHeight: "100%", overflowY: "auto" }}>
        {children}
      </Content>
    </AntdLayout>
  );
};
