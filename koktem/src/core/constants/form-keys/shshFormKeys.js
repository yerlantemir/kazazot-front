const net_calorific_value =
  "теплота сгорания низшая, МДж/м3, при 20С, 101,325 КПа, не менее";
const wobbe_number_range = "область значения числа Воббе, МДж/м3";
const mass_concentration_of_hydrogen_sulfide =
  "массовая концентрация сероводорода, г/м3, не более";
const mass_concentration_of_mercapton_sulfur =
  "массовая концентрация меркаптановой серы, г/м3, не более";
const oxygen_mole_fraction = "молярная доля кислорода";
const mechanical_impurities_mass =
  "масса механических примесей в 1 м3, г, не более";
const metan = "метан, %";
const etan = "этан, %";
const propan = "пропан, %";
const izobytan = "изо-бутан, %";
const nbytan = "н-бутан, %";
const izopentan = "изо-пентан, %";
const npentan = "н-пентан, %";
const geksan = "гексаны, %";
const geptan = "гептаны, %";

const hydrogen = "водород, %";
const carbon_manoxide = "окись углерода, %";
const carbon_dioxided = "двуокись углерода, %, не более";
const azot = "азот, %";
const helium = "гелий, %";
const water_dew_point_200C =
  "точка росы по воде, С, при t=20С и P газа=40,6 кгс/см2";
const water_dew_point_220C =
  "точка росы по воде, С, при t=22С и P газа=40,0 кгс/см2, не выше";
const carbohydrate_dew_point =
  "точка росы по углеводам, С, t=0C и P газа= кгс/см2, не выше";
const density = "плотность, кг/м3, при при t=20С и P=101,325 кПа (расчетная)";

const shshDict = {
  net_calorific_value,
  wobbe_number_range,
  mass_concentration_of_hydrogen_sulfide,
  mass_concentration_of_mercapton_sulfur,
  oxygen_mole_fraction,
  mechanical_impurities_mass,
  metan,
  etan,
  propan,
  izobytan,
  nbytan,
  izopentan,
  npentan,
  geksan,
  geptan,
  hydrogen,
  carbon_manoxide,
  carbon_dioxided,
  azot,
  helium,
  water_dew_point_200C,
  water_dew_point_220C,
  carbohydrate_dew_point,
  density,
};

const list = Object.values(shshDict);
export { list, shshDict };
