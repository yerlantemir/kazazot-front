const accessToken = "accessToken";
const role = "role";
const id = "id";
export default {
  accessToken,
  role,
  id,
};
