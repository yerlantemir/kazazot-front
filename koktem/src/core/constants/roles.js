const ecolog = "ecolog";
const accountant = "accountant";
const shsh = "ShSh";
const mainInstrumentOperator = "mainInstrumentOperator";
export default {
  ecolog,
  accountant,
  shsh,
  mainInstrumentOperator,
};
