import axios from "axios";
import localStorageKeys from "../constants/localStorageItemKey.js";

const configuredAxios = axios.create({
  // eslint-disable-next-line no-undef
  baseURL: process.env.REACT_APP_ENDPOINT_URL,
  headers: { "Content-Type": "application/json" },
});

configuredAxios.interceptors.request.use((config) => {
  const key = localStorageKeys.accessToken;
  const accessToken = localStorage.getItem(key);
  if (accessToken) {
    config.headers["Authorization"] = `Bearer ${accessToken}`;
  } else {
    window.location.href = `localhost:3000`;
  }
  return config;
});

export default configuredAxios;
