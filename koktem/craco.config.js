const CracoLessPlugin = require("craco-less")

module.exports = {
    plugins: [
      {
        plugin: CracoLessPlugin,
        options: {
          lessLoaderOptions: {
            lessOptions: {
              modifyVars: { '@primary-color': '#4DB058' },
              javascriptEnabled: true,
            },
        },
    },
    },
],
};
  